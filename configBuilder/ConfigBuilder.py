#!/usr/bin/pyhton3

# import stuff
from tkinter import Tk
import re



def printHelp():
    print("\n\nPaste Lines in format:\nBuilding DescName Floor VRF   VLAN     NetADDR        Prefix    EXAMPLE:\nBT        kiosk   I875  gnet 1606     10.251.1.128     26\nWhere each is seperated by either a space or a tab and each line\nis seperated by a return")
#Var to hold input settings lines
setLines = ''
def getLines():
    end = ""
    print ("\n\ninput Lines:")
    global setLines
    #getting raw input on py 3 and later is simply input {py 2.x | setLines = '\n'.join(iter(raw_input,end)) }{py 3.x | setLines = '\n'.join(iter(input,end))}
    setLines = '\n'.join(iter(input,end))



#Finding Subnetmask
#pref will be CIDR mask pref, outputs mask with given pref
def prefix_eval(pref):
    Maskbank = ["0.0.0.0","128.0.0.0","192.0.0.0","224.0.0.0","240.0.0.0","248.0.0.0","252.0.0.0","254.0.0.0","255.0.0.0","255.128.0.0",
    "255.192.0.0","255.224.0.0","255.240.0.0","255.248.0.0","255.252.0.0","255.254.0.0","255.255.0.0","255.255.128.0","255.255.192.0",
    "255.255.224.0","255.255.240.0","255.255.248.0","255.255.252.0","255.255.254.0","255.255.255.0","255.255.255.128","255.255.255.192",
    "255.255.255.224","255.255.255.240","255.255.255.248","255.255.255.252","255.255.255.254","255.255.255.255"]
    return Maskbank[pref]

#adds app to the last octet of the given ip as string
def Addip(ip,app):
    address = ip.split(".")
    #address = ip(255.255.255.255)=[255,255,255,255]
    #add an Integer "app" to the last octet
    address[3]=str(int(address[3])+app)
    #Reconstruct ip by adding first octet
    Output = address[0]
    #then loop to add the rest of the octets  
    i=1
    while i<4:
        Output = Output+'.'+address[i]
        i=i+1
    return Output


#build SVI 1
#Bldn   DescN    Flor    VRF     VLAN   NetADDR          Prefix
#Line[0],Line[1],Line[2],Line[3],Line[4],Line[5],       Line[6]      Subnet mask = mask
#ISE and Mcast are type Boolean
#Both switches get the same VLAN config:
#Example:
#vlan 1883
#name 172.27.142.0/23_prod_I823
#"vlan"+Line[4]+"/n name "+Line[5]+"/"+prefix"+"_"+Line[3}+"_"+Line[4]
def printSVI1(Line,ISE,Mcast,flowM):
    #create Mask
    prefix = int(Line[6])
    mask = prefix_eval(prefix)

    #add each line individually to a list to build with specifics
    Output = []
    Output.append("interface vlan"+Line[4])
    Output.append("Description " + Line[0] + '-' + Line[2] + " " + Line[1])
    Output.append("ip vrf forwarding " + Line[3])
    Output.append("ip address " + Addip(Line[5],2) + ' ' + mask)
    #add Helper ip-address
    #Yes ISE
    if Line[1].lower() == "secnet":
        Output.append("ip helper-address 129.255.112.10")
    if Line[1].lower() in ["prod","prodnet",  "mednet", "mednet1", "mednet2", "mednet3", "voip", "imagnet", "facnet", "pcinet","voipnet","patnet"] and ISE:
        Output.append("ip helper-address 129.255.112.10\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() == "phlps" and ISE:
        Output.append("ip helper-address 10.40.65.83\nip helper-address 129.255.127.22")
        #old: Output.append("ip helper-address 10.107.199.8\nip helper-address 10.107.199.9\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() in ["kiosk","kiosknet","hold","holdnet"] and ISE:
        Output.append("ip helper-address 129.255.126.164\nip helper-address 129.255.126.165\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
        #OLD: Output.append("ip helper-address 129.255.1.230\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() in ["mobnet","mgmt"] and ISE:
        Output.append("ip helper-address 129.255.112.10")
    #Not ISE
    if Line[1].lower() in ["prod","prodnet", "mednet", "mednet1", "mednet2", "mednet3", "voip", "imagnet", "facnet","pcinet","voipnet"] and not ISE:
        Output.append("ip helper-address 129.255.112.10\nip helper-address 172.26.12.108")
    if Line[1].lower() == "phlps" and not ISE:
        Output.append("ip helper-address 10.107.199.8\nip helper-address 10.107.199.9\nip helper-address 172.26.12.108")
    if Line[1].lower() in ["kiosk","kiosknet","hold","holdnet"] and not ISE:
        Output.append("ip helper-address 129.255.126.164\nip helper-address 129.255.126.165\nip helper-address 172.26.12.108")
        #OLD Output.append("ip helper-address 129.255.1.230\nip helper-address 172.26.12.108")
    if Line[1].lower() in ["mobnet","mgmt","patnet"] and not ISE:
        Output.append("ip helper-address 129.255.112.10")
    Output.append("no ip proxy-arp\nno ip redirects")

    if(Mcast):
        Output.append("ip pim redundancy HSRP"+Line[4]+" hsrp dr-priority 200")
        Output.append("ip pim sparse-mode")
    if(flowM):
        Output.append("ip flow monitor UIHC-FLOWMONITOR input")
        Output.append("ip flow monitor UIHC-FLOWMONITOR output")
    if Line[1].lower() == "holdnet":
        Output.append("ip access-group HOLD-VLAN-IN in")
    if Line[1].lower() == "mobnet":
        Output.append("ip access-group MobNet outip pim sparse-dense-mode")
    if Line[1].lower() == "prod" or Line[1] == "secnet":
        Output.append("ip pim sparse-dense-mode")
    if Line[1].lower() == "phlps" or Line[1] == "patnet":
        Output.append("ip pim sparse-dense-mode\nip pim query-interval 1")
    if Line[1].lower() == "patnet":
        Output.append("ip igmp version 3")
    Output.append("standby version 2\nstandby "+Line[4]+" ip "+ Addip(Line[5],1))
    if (int(Line[4]) % 2) == 0:
        Output.append("standby "+Line[4]+" priority 180")
    else:
        Output.append("standby "+Line[4]+" priority 200")
    Output.append("standby "+Line[4]+" preempt delay minimum 30\nstandby "+Line[4]+" authentication md5 key-string MmTark:2652.")
    Output.append("standby "+Line[4]+" name HSRP"+Line[4])
    if (int(Line[4]) % 2) != 0:
        Output.append("standby "+Line[4]+" track 1 decrement 15")
        Output.append("standby "+Line[4]+" track 2 decrement 15")
    Output.append("no shut\nexit\n\n")


    #output the List Line-by-Line
    Cliptest = ''
    for each in Output:
        Cliptest = Cliptest+'\n'+each
    return Cliptest



#SVI 2 vs SVI 1 might not be correct
#build SVI 2
#Bldn   DescN    Flor    VRF     VLAN   NetADDR          Prefix
#Line[0],Line[1],Line[2],Line[3],Line[4],Line[5],       Line[6]      Subnet mask = mask
#ISE and Mcast are type Boolean
def printSVI2(Line,ISE,Mcast,flowM):
    #create Mask
    prefix = int(Line[6])
    mask = prefix_eval(prefix)
    #add each line individually to a list to build with specifics
    Output = []
    Output.append("interface vlan"+Line[4])
    Output.append("Description " + Line[0] + '-' + Line[2] + " " + Line[1])
    Output.append("ip vrf forwarding " + Line[3])
    Output.append("ip address " + Addip(Line[5],3) + ' ' + mask)
    #add Helper ip-address
    #Yes ISE
    if Line[1].lower() == "secnet":
        Output.append("ip helper-address 129.255.112.10")
    if Line[1].lower() in ["prod","prodnet" , "mednet", "mednet1", "mednet2", "mednet3", "voip", "imagnet", "facnet", "pcinet" , "voipnet","patnet"] and ISE:
        Output.append("ip helper-address 129.255.112.10\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() == "phlps" and ISE:
        Output.append("ip helper-address 10.40.65.83\nip helper-address 129.255.127.22")
        #old: Output.append("ip helper-address 10.107.199.8\nip helper-address 10.107.199.9\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() in ["kiosk","kiosknet","hold","holdent"] and ISE:
        Output.append("ip helper-address 129.255.126.164\nip helper-address 129.255.126.165\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
        #OLD: Output.append("ip helper-address 129.255.1.230\nip helper-address 172.26.12.111\nip helper-address 129.255.127.22")
    if Line[1].lower() in ["mobnet","mgmt"] and ISE:
        Output.append("ip helper-address 129.255.112.10")
    #Not ISE
    if Line[1].lower() in ["mobnet","mgmt","patnet"] and not ISE:
        Output.append("ip helper-address 129.255.112.10")
    if Line[1].lower() in ["prod","prodnet", "mednet", "mednet1", "mednet2", "mednet3", "voip", "imagnet", "facnet","pcinet","voipnet"] and not ISE:
        Output.append("ip helper-address 129.255.112.10\nip helper-address 172.26.12.108")
    if Line[1].lower() == "phlps" and not ISE:
        Output.append("ip helper-address 10.107.199.8\nip helper-address 10.107.199.9\nip helper-address 172.26.12.108")

    if Line[1].lower() in ["kiosk","kiosknet","hold","holdnet"] and not ISE:
        Output.append("ip helper-address 129.255.126.164\nip helper-address 129.255.126.165\nip helper-address 172.26.12.108")
        #OLD Output.append("ip helper-address 129.255.1.230\nip helper-address 172.26.12.108")    
    if Line[1].lower() in ["kiosk"]:
        Output.append("ip access-group ingress-wired-kiosk in")
    if Line[1].lower() in ["hold"]:
        Output.append("ip access-group HOLD-VLAN-IN in")
    Output.append("no ip proxy-arp\nno ip redirects")
    if Line[1].lower() in ["kiosk"]:
        Output.append("ip access-group ingress-wired-kiosk in")
    if Line[1].lower() in ["hold"]:
        Output.append("ip access-group HOLD-VLAN-IN in")


    if(Mcast):
        Output.append("ip pim redundancy HSRP"+Line[4]+" hsrp dr-priority 200")
        Output.append("ip pim sparse-mode")
    if(flowM):
        Output.append("ip flow monitor UIHC-FLOWMONITOR input")
        Output.append("ip flow monitor UIHC-FLOWMONITOR output")



    if Line[1].lower() == "holdnet":
        Output.append("ip access-group HOLD-VLAN-IN in")
    if Line[1].lower() == "mobnet":
        Output.append("ip access-group MobNet outip pim sparse-dense-mode")
    if Line[1].lower() == "prod" or Line[1] == "secnet":
        Output.append("ip pim sparse-dense-mode")
    if Line[1].lower() == "phlps" or Line[1] == "patnet":
        Output.append("ip pim sparse-dense-mode\nip pim query-interval 1")
    if Line[1].lower() == "patnet":
        Output.append("ip igmp version 3")
    Output.append("standby version 2\nstandby "+Line[4]+" ip "+ Addip(Line[5],1))
    if (int(Line[4]) % 2) != 0:
        Output.append("standby "+Line[4]+" priority 180")
    else:
        Output.append("standby "+Line[4]+" priority 200")
    Output.append("standby "+Line[4]+" preempt delay minimum 30\nstandby "+Line[4]+" authentication md5 key-string MmTark:2652.")
    Output.append("standby "+Line[4]+" name HSRP"+Line[4])
    if (int(Line[4]) % 2) == 0:
        Output.append("standby "+Line[4]+" track 1 decrement 15")
        Output.append("standby "+Line[4]+" track 2 decrement 15")
    Output.append("no shut\nexit\n\n")
    #output the List Line-by-Line
    Cliptest = ''
    for each in Output:
        Cliptest = Cliptest+'\n'+each
        #print each
    return Cliptest


#switch is int 1 or 2 to pick switch config span tree section
#get all vlans from lines, Sorted, for later outputting spanning tree sets
#returns String list of vlans in sorted order : ["0","1","2",..."4096"]
#even first odd second
#switch 1 even prio: 4096 odd:0
#switch 2 even prio: 0 odd:4096
def collectSpanVlan(LineSet ,switch):
    even_set = []
    odd_set = []
    output = ""
    for rawLine in LineSet:
        Line = re.split('\t+| +',rawLine) #Split line by tab,spaces or | 
        #Bldn   DescN    Flor    VRF     VLAN   NetADDR          Prefix
        #Line[0],Line[1],Line[2],Line[3],Line[4],Line[5],       Line[6]      Subnet mask = mask
        if (int(Line[4])) % 2 ==0:
            if Line[3] not in even_set:
                even_set.append(Line[4])
        else:
            if Line[4] not in odd_set:
                odd_set.append(Line[4])
    #sort sets
    even_set.sort(key=int)
    odd_set.sort(key=int)
    
    if switch == 1:
        prioEven = "4096"
        prioOdd = "0"
    else:
        prioEven = "0"
        prioOdd = "4096"
    #SET OF EVEN SPANNING TREE VLAN COMES FIRST
    #spanning-tree vlan 1111,1111,1111,1111,1111,1111,1111,1111,1111 priority 4096

    setIndex = 0
    if(len(even_set)>0):
        output = output +"spanning-tree vlan "
        while(setIndex < len(even_set)-1):
            output= output + even_set[setIndex] + ","
            setIndex = setIndex + 1
        
        setIndex = setIndex
        output = output + even_set[setIndex] + " "
        output = output +"priority " + prioEven+ "\n"


    setIndex = 0
    if(len(odd_set)>0):
        output = output +"spanning-tree vlan "
        while(setIndex < len(odd_set)-1):
            output= output + odd_set[setIndex] + ","
            setIndex = setIndex + 1
        
        setIndex = setIndex
        output = output + odd_set[setIndex] + " "
        output = output +"priority " + prioOdd+ "\n"

        

        
            

    return output

    
    
    
#ouptup Bgp lines
def printBgp(Lineset):
    #list of [vrf,ipcollecion] lists with vrf and their corresponding ip collections to be printed in
    #a later loop
    bgpRef = []
    #known is the list of vrf in our bgpRef
    known = []


    for rawLine in Lineset:
        Line = re.split('\t+| +',rawLine) #Split line by tab,spaces or | 
        #Bldn   DescN    Flor    VRF     VLAN   NetADDR          Prefix
        #Line[0],Line[1],Line[2],Line[3],Line[4],Line[5],       Line[6]      Subnet mask = mask
        if Line[3] not in known:
            #create new vrf listing if not in Known vrf list
            known.append(Line[3])
            bgpRef.append([Line[3]," network "+Line[5]+" mask "+prefix_eval(int(Line[6]))])
        else:
            #each vrf will be in a [vrf,ipcollection] format (both strings)
            for vrf in bgpRef:

                if Line[3]==vrf[0]:
                    #add the network to the vrf String
                    vrf[1] = vrf[1] + "\n network "+Line[5] + " mask "+prefix_eval(int(Line[6]))
    output = ""
    print("\n\n\n\n\n")  #buffer for readability
    output = output + "router bgp 65010\n"
    print("router bgp 65010\n")

    for each in bgpRef:
        output = output + "address-family ipv4 vrf "+each[0]+"\n"
        print("address-family ipv4 vrf "+each[0])
        output = output + each[1]+"\n"
        print(each[1])
        output = output + "exit-address-family\n"
        print("exit-address-family")
    return output 




#Print and send to clipboard config blocks with settings
def clipPrint():

    #ask ISE
    ISE = True

    check = False   # for ending the while loop when proper selection made
    while not check:
        query = input("ISE?\nInput [y/n]:\n")
        query = query.lower()
        if query == "y":
            ISE = True
            check = True
        elif query == "n":
            ISE = False
            check = True
        else:
            print("\nFormatting err Only 'y' or 'n' allowed")
    #ask MultiCast
    Mcast = True

    check = False    # for ending the while loop when proper selection made
    while not check:
        query = input("Multicast Needed?\nInput [y/n]:\n")
        query = query.lower()
        if query == "y":
            Mcast = True
            check = True
        elif query == "n":
            Mcast = False
            check = True
        else:
            print("\nFormatting err Only 'y' or 'n' allowed")
            
    #ask flow monitor
    flowM = True

    check = False    # for ending the while loop when proper selection made
    while not check:
        query = input("flow monitor Needed?\nInput [y/n]:\n")
        query = query.lower()
        if query == "y":
            flowM = True
            check = True
        elif query == "n":
            flowM = False
            check = True
        else:
            print("\nFormatting err Only 'y' or 'n' allowed")
    

    #start Strings to be sent to clipboard
    #"vlan"+Line[4]+"/n name "+Line[5]+"/"+prefix"+"_"+Line[3}+"_"+Line[4]
    vlan_coll = ''
    SVI1 = '' 
    SVI2 = ''
    
    #read then output in order of input
    eaLine = setLines.split('\n')
    for rawLine in eaLine:
        Line = re.split('\t+| +',rawLine) #Split line by tab,spaces or | 
        #Line = [c.strip() for c in Line]     //Strips but decided we might not need
        #Convert descName to lower for easier handling in SVI
        #Line[1] = Line[1].lower()

        #check for formatting
        if len(Line) != 7:
            #Debugging
            print("Line("+rawLine+") invalid Format errorLength\n            Discarded from output")
            continue
        #TODO
        #ideas to err check: ip formatting, vlan formatting, prefix, Floor formatting
        Vlan = Line[4]
        #CHECK VLAN is  between 2-4094
        if (int(Vlan)>4094) or (int(Vlan)<2):
            print("Line("+rawLine+") invalid Format errorVlan\n            Discarded from output")
            continue
        floor = Line[2]
        x = ''
        if str.lower(list(floor)[0]) != "i" or int(x.join(list(floor)[1:]))>999:
            print("Line("+rawLine+") invalid Format errorFloor\n            Discarded from output")
            continue
        #append another config block to Clipboard string
        vlan_coll = vlan_coll + "vlan "+Line[4]+"\n name "+Line[5]+"/"+Line[6]+"_"+Line[1]+"_"+Line[2]+"\n"
        SVI1 = SVI1 + printSVI1(Line,ISE,Mcast,flowM)
        SVI2 = SVI2 + printSVI2(Line,ISE,Mcast, flowM)
    SVI1 = SVI1 + collectSpanVlan(eaLine,1) 
    SVI2 = SVI2 + collectSpanVlan(eaLine,2)
    query = ' '
    while query != "ex":
        query = input("\n\n\nTo copy Switch 1 SVI1 config to Clipboard input '1'\nTo copy switch 2 SVI2 config to Clipboard input '2'\nTo copy BGP lines input 'bgp'\nTo Exit input 'ex'\n")
        
        if query == '1':
         #use Tkinter to open, clear then add config to clipboard
            print(vlan_coll+SVI1)
            #r = Tk()
            #r.withdraw()
            #r.clipboard_clear
            #r.clipboard_append(vlan_coll+SVI1)
            #r.update()
            #r.after(100, r.destroy)
            print("\n\n\n\n^^^Blocks \nWith Settings:[ISE:",ISE,"][MCAST:",Mcast,"]\n")
        if query == '2':
            #use Tkinter to open, clear then add config to clipboard
            print(vlan_coll+SVI2)
            #r = Tk()
            #r.withdraw()
            #r.clipboard_clear
            #r.clipboard_append(vlan_coll+SVI2)
            #r.update()
            #r.after(100, r.destroy)
            print("\n\n\n\n^^^Blocks \nWith Settings:[ISE:",ISE,"][MCAST:",Mcast,"]\n")
        if query == 'bgp':
            bgp = printBgp(eaLine)
            #r = Tk()
            #r.withdraw()
            #r.clipboard_clear
            #r.clipboard_append(bgp)
            #r.update()
            #r.after(100, r.destroy)
            print("\n\n\n\n^^^Blocks \nWith Settings:[ISE:",ISE,"][MCAST:",Mcast,"]\n")
            



        #print "\""query+"\" is not a valid option\n"




printHelp()   #initial prompt for lines with help message
getLines()     #get lines
clipPrint()    #Set output settings and format lines and Send to Clipboard





#exit loop with options to change line settings or re input lines
query = ' '
while query != "ex":
    query = input("\n\n\nTo edit setting the same Lines input 're'\nTo use a diff set of Lines input 'new'\nTo view input formatting 'help'\nTo Exit input 'ex'\n")
    if query == 'help':
        printHelp()
        continue
    if query == 're':
        clipPrint()
    if query == 'new':
        getLines()
        clipPrint()
